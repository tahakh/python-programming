import pandas as pd
from rpy2.robjects import r, pandas2ri

# Clear the Environment
r("rm(list=ls())")

# Load all library files
r("""
library(edgeR)
library(gplots)
library(RColorBrewer)
library(ggplot2)
library(ggpubr)
library(pheatmap)
library(gridExtra)
library(formatR)
library(tidyverse) 
library(reshape)
library(ggeasy)
""")

# Load Enhancer CAGE count files
en_cage_count = pd.read_table("Data/Enhancer Expression Data/Comprehensive_enhancer_countmatrix.txt")

# Design the sample info
group_info = pd.Series(["CTRL"]*9 + ["MA"]*15 + ["SA"]*13)

# Create a DGEList data object
dgeFull = r["DGEList"](counts=en_cage_count, group=group_info)

# Design the model matrix
design = r["model.matrix"](~0+group_info, data=dgeFull$samples)

# Make a contrast
my_contrasts = r["makeContrasts"](MAvsCtrl="group_infoMA-group_infoCTRL",
                                  SAvsCtrl="group_infoSA-group_infoCTRL",
                                  SAvsMA="group_infoSA-group_infoMA", 
                                  levels=design)

# Estimate the dispersion (common and tagwise both)
dgeFull = r["estimateGLMCommonDisp"](dgeFull, design=design)
dgeFull = r["estimateGLMTagwiseDisp"](dgeFull, design=design)

# To perform likelihood ratio test to test for differential expression
fit = r["glmFit"](dgeFull, design)

# Define Control and Case
all_CTRL = list(range(1, 10))
all_MA = list(range(10, 25))
all_SA = list(range(25, 38))

# Enhancer related gene
Enhancer_Annotation = pd.read_table("Data/Annotation/Enhancer_annotation.txt", header=None)
Enhancer_Annotation = Enhancer_Annotation.drop_duplicates(subset=[0])
Enhancer_Annotation = Enhancer_Annotation[~Enhancer_Annotation[1].isin(["TRNAN-GUU"])]
Enhancer_Annotation = Enhancer_Annotation.set_index(0)

# Load TPM data
pandas2ri.activate()
r["load"]("Data/Enhancer Expression Data/Comprehensive_enhancer_tpmmatrix.RData")
Asthmaenhancer_tpm8289 = pandas2ri.ri2py(r["Asthmaenhancer_tpm8289"])

# Compare Severe vs Control group
qlf_ctrl_sa = r["glmLRT"](fit, contrast=my_contrasts[,"SAvsCtrl"])
qlf_ctrl_sa_toptags = r["topTags"](qlf_ctrl_sa, n=float("inf"))

toptags_table = pandas2ri.ri2py(qlf_ctrl_sa_toptags.rx2("table"))

# Set thresholds
print(toptags_table[toptags_table["FDR"] < 0.2].shape[0])

# Convert pandas DataFrame to R data.frame
enhancer_annotation_r = pandas2ri.py2ri(Enhancer_Annotation.reset_index())
r["colnames"](enhancer_annotation_r)[0] = "V1"

# Load Asthma related genes from the GWAS catalog
Childhood_onset_asthma = pd.read_table("Data/Enhancer Expression Data/Childhood_Asthma_gene.txt")
New_gene = pd.read_table("Data/Enhancer Expression Data/Asthma_gene.txt")

print(set(Childhood_onset_asthma["V1"]).intersection(toptags_table["Gene"]))
print(set(New_gene["V1"]).intersection(toptags_table["Gene"]))

# Figure 2(B)
asthma_gene = toptags_table[toptags_table["Gene"].isin(New_gene["V1"])]

# Figure 2(C)
vol_text_MACtrl = MACtrl_gene[MACtrl_gene["FDR"] < 0.3]

# Figure 2(D)
vol_text_SAMA = SAMA_gene

# Zscore function
Z_Score = r("""
function(mat, dim=NULL) {
  if(is.null(dim)) {
    z_mat <- (mat - mean(mat, na.rm=TRUE))/sd(mat, na.rm=TRUE)
  } else {
    if(is.matrix(mat)) {
      if(dim == 2) mat <- t(mat)
      r_means <- rowMeans(mat, na.rm=TRUE)
      r_sd <- apply(mat, 1, sd, na.rm=TRUE)
      z_mat <- (mat - r_means)/r_sd
      if(dim == 2) z_mat <- t(z_mat)
    } else stop('Invalid Matrix Object !!!')
  }
  return(z_mat)  
}
""")

# Heatmap severe vs. mild
Zscore_SA_MA = Zscore_top_enhancer.loc[set_SA,]
Zscore_SA_MA_z = r["Z_Score"](Zscore_SA_MA)

# Heatmap of a subset of patients showing the expression of the 300 enhancers
r["pheatmap"](Zscore_SA_MA_z, cluster_rows=False, cluster_cols=False, 
              main="Top 300 enhancers based on significant MAvsCtrl", fontsize=7)
