import pandas as pd
import numpy as np
from rpy2.robjects.packages import importr
from rpy2.robjects import r
from rpy2.robjects import pandas2ri

# Activate pandas conversion
pandas2ri.activate()

# Import R packages
edgeR = importr("edgeR")
gplots = importr("gplots")
RColorBrewer = importr("RColorBrewer")
ggplot2 = importr("ggplot2")
ggpubr = importr("ggpubr")
pheatmap = importr("pheatmap")
gridExtra = importr("gridExtra")
formatR = importr("formatR")
tidyverse = importr("tidyverse")
reshape = importr("reshape")
ggeasy = importr("ggeasy")

# Load data
cage_count_table = pd.read_rdata("Data/TC Expression Data/RobustCAGE_count.RData")
cage_tpm = pd.read_rdata("Data/TC Expression Data/RobustCAGE_tpm.RData")

# Design sample info
group_info = pd.Series(["CTRL"] * 9 + ["MA"] * 15 + ["SA"] * 13)

# Create DGEList data object
dgeFull = edgeR.DGEList(counts=cage_count_table, group=group_info)

# Design the model matrix
design = edgeR.model_matrix(~0 + group_info)

# Make a contrast
my_contrasts = edgeR.makeContrasts(MAvsCtrl="group_infoMA-group_infoCTRL",
                                   SAvsCtrl="group_infoSA-group_infoCTRL",
                                   SAvsMA="group_infoSA-group_infoMA",
                                   levels=design)

# Estimate the dispersion
dgeFull = edgeR.estimateGLMCommonDisp(dgeFull, design=design)
dgeFull = edgeR.estimateGLMTagwiseDisp(dgeFull, design=design)

# Perform likelihood ratio test
fit = edgeR.glmFit(dgeFull, design)

# Define Control and Case
all_CTRL = list(range(1, 10))
all_MA = list(range(10, 25))
all_SA = list(range(25, 38))

# Load TPM data
RobustCAGE_tpm = pd.read_rdata("Data/TC Expression Data/RobustCAGE_tpm.RData")

# Load Asthma related genes from the GWAS catalog
GWAS_hits_all = pd.read_csv("Data/TC Expression Data/DGE_GWAS_hits.txt", sep="\t", dtype=str)
GWAS_hits_subset = pd.read_csv("Data/TC Expression Data/DGE_Pubmed_hits.txt", sep="\t", dtype=str)

# Merge datasets
GWAS_hits_all = GWAS_hits_all.merge(GWAS_hits_subset, on="DGE.promoter", how="left")

# Filter rows with GWAS hits
GWAS_hits_all = GWAS_hits_all[GWAS_hits_all["GWAS.hits.update"].notna()]

# Compare Severe vs Control group
qlf_ctrl_sa = edgeR.glmLRT(fit, contrast=my_contrasts[, "SAvsCtrl"])
qlf_ctrl_sa_toptags = edgeR.topTags(qlf_ctrl_sa, n=float("inf"))

# Extract table
toptags_table_SACTRL = qlf_ctrl_sa_toptags[0]

# Filter based on threshold
filter_SA_Ctrl = toptags_table_SACTRL[toptags_table_SACTRL["FDR"] < 0.1]

# Make table for plotting
filter_SA_FDR = filter_SA_Ctrl[["FDR", "logFC"]]
filter_SA_FDR["threshold"] = np.where((filter_SA_FDR["logFC"] > 0.1) & (filter_SA_FDR["FDR"] < 0.1), "up",
                                      np.where((filter_SA_FDR["logFC"] < -0.1) & (filter_SA_FDR["FDR"] < 0.1), "down", "NS"))

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec
from matplotlib_venn import venn2
from adjustText import adjust_text

# Define the plotting function for volcano plots
def plot_volcano(df, title):
    # Subset data for significant points
    vol_text = df[df['FDR'] < 0.1]
    
    # Define colors based on threshold
    colors = np.where(df['threshold'] == 'up', 'deepskyblue3', 
                      np.where(df['threshold'] == 'down', 'deepskyblue3', 'lightgrey'))
    
    # Plot the volcano plot
    plt.figure(figsize=(8, 6))
    plt.scatter(df['logFC'], -np.log10(df['FDR']), c=colors, s=50, alpha=0.7, edgecolors='none')
    
    # Add significance line
    plt.axhline(y=1, color='#990000', linestyle='--')
    
    # Add text labels for significant genes
    for i, txt in enumerate(vol_text['Gene']):
        plt.annotate(txt, (vol_text['logFC'].iloc[i], -np.log10(vol_text['FDR'].iloc[i])),
                     xytext=(5, 5), textcoords='offset points', size=10)
    
    # Add FDR < 0.1 label
    plt.text(5, 0.8, 'FDR < 0.1', color='#990000', size=12)
    
    # Set axis labels and title
    plt.xlabel('Expression change (log2FoldChange)')
    plt.ylabel('Significance (-log10FDR)')
    plt.title(title)
    
    # Set axis limits
    plt.xlim(-3, 6)
    plt.ylim(0, 7)
    
    # Show plot
    plt.grid(False)
    plt.show()

# Plot for Severe vs. Control
plot_volcano(filter_SA_FDR, "Severe vs. Control")

# Plot for Mild vs. Control
plot_volcano(filter_0.05_MACtrl, "Mild vs. Control")

# Plot for Severe vs. Mild
plot_volcano(filter_SA_MA_FDR, "Severe vs. Mild")

# Extracted Gene Lists
print(TCs_DEgene.head())

# Save Gene lists
TCs_DEgene.to_csv("Data/TCs_DEgene.txt", sep='\t', index=False, header=False)
# Extract Gene Ontology (GO) enrichment analysis for DE genes
import gseapy as gp

# Define a function for GO enrichment analysis
def run_GO_enrichment(gene_list, title):
    # Run GO enrichment analysis using gseapy
    results = gp.enrichr(gene_list=gene_list, description=title, gene_sets='GO_Biological_Process_2018',
                         cutoff=0.5)  # You can adjust the cutoff value as needed
    
    # Plot the top enriched GO terms
    results.res2d.head(10).plot.barh(y='Term', x='Adjusted P-value', color='lightblue', figsize=(10, 6))
    plt.xlabel('Adjusted P-value')
    plt.ylabel('GO Term')
    plt.title('Top 10 Enriched GO Terms - ' + title)
    plt.show()

# Run GO enrichment analysis for TCs_DEgene
run_GO_enrichment(TCs_DEgene['Gene'], "Differentially Expressed Genes in COVID-19 vs. Control")

# Run GO enrichment analysis for MA_DEgene
run_GO_enrichment(MA_DEgene['Gene'], "Differentially Expressed Genes in Moderate Asthma vs. Control")

