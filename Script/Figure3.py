import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import linregress
from statsmodels.stats.multitest import multipletests

# Clear the Environment
# No equivalent in Python, as each variable must be cleared individually

# Load all library files
# No need to explicitly load libraries in Python

# Load Enhancer and Promoter TPM data and also correlation data
TCs_tpm_mat = pd.read_pickle("Data/TC Expression Data/RobustCAGE_tpm_IDchange.pkl")
Enhancers_tpm_mat = pd.read_pickle("Data/Enhancer Expression Data/Comprehensive_enhancer_tpmmatrix.pkl")

prom_enha_cor_mat_all = pd.read_pickle("Data/Supplementary/Correlation Table/prom_enha_cor_mat_all.pkl")
prom_enha_cor_pval_mat_all = pd.read_pickle("Data/Supplementary/Correlation Table/prom_enha_cor_pval_mat_all.pkl")

# Define Control and Case
all_CTRL = list(range(1, 10))
all_MA = list(range(10, 25))
all_SA = list(range(25, 38))

# CXCL1 gene TCs = chr4:74735101-74735247 Enhancer1 = chr4:74738489-74738740 Enhancer2 = chr4:74740108-74740484

# Figure 3 (TCs and Enhancer 1)

TCs = "chr4:74735101-74735247"
Enh = "chr4:74738489-74738740"

TC_mat = TCs_tpm_mat.loc[TCs, all_CTRL + all_MA + all_SA].to_frame().T
Enh_mat = Enhancers_tpm_mat.loc[Enh, all_CTRL + all_MA + all_SA].to_frame().T

Cor_plot = pd.concat([TC_mat, Enh_mat]).reset_index(drop=True)
Cor_plot["Sample"] = ["Ctrl"] * len(all_CTRL) + ["MA"] * len(all_MA) + ["SA"] * len(all_SA)

# Check correlation value
value_cor = prom_enha_cor_mat_all.loc[TCs, Enh]
print("Correlation:", value_cor)

# Check p-value
value_pvalue = prom_enha_cor_pval_mat_all.loc[TCs, Enh]
print("P-value:", value_pvalue)

# Correlation plot
sns.set_style("whitegrid")
g_Cor_Enh1 = sns.lmplot(data=Cor_plot, x="chr4:74735101-74735247", y="chr4:74738489-74738740",
                        hue="Sample", palette={"Ctrl": "darkred", "MA": "green", "SA": "gold"},
                        fit_reg=True, scatter_kws={"s": 50}, ci=None)
g_Cor_Enh1.set_axis_labels("Expression in promoter (TPM)", "Expression in enhancer (TPM)")
g_Cor_Enh1.fig.suptitle("Correlation = 0.75\nP-value = 9.085531e-08")
plt.show()

# Boxplot in TCs and Enhancer-1
TC_boxplot = pd.concat([TC_mat["chr4:74735101-74735247"], Cor_plot["Sample"]], axis=1)
TC_boxplot.columns = ["Value", "Sample"]
TC_boxplot["Sample"] = pd.Categorical(TC_boxplot["Sample"], categories=["Ctrl", "MA", "SA"], ordered=True)

g_TC = sns.boxplot(data=TC_boxplot, x="Sample", y="Value", palette={"Ctrl": "darkred", "MA": "green", "SA": "gold"})
g_TC = sns.stripplot(data=TC_boxplot, x="Sample", y="Value", color="black", alpha=0.5)
g_TC.set(xlabel="Group", ylabel="Promoter expression (TPM)")
plt.title("Boxplot of Promoter expression (TPM) in TCs")
plt.show()

# Fold change in CXCL1 TCs
Fc_TCs = [TC_boxplot[TC_boxplot["Sample"] == "Ctrl"]["Value"].mean() / TC_boxplot[TC_boxplot["Sample"] == "MA"]["Value"].mean(),
          TC_boxplot[TC_boxplot["Sample"] == "Ctrl"]["Value"].mean() / TC_boxplot[TC_boxplot["Sample"] == "SA"]["Value"].mean(),
          TC_boxplot[TC_boxplot["Sample"] == "SA"]["Value"].mean() / TC_boxplot[TC_boxplot["Sample"] == "MA"]["Value"].mean()]
print("Fold change in CXCL1 TCs:", Fc_TCs)

# Fold change in Enhancer-1
Enh_boxplot = pd.concat([Enh_mat["chr4:74738489-74738740"], Cor_plot["Sample"]], axis=1)
Enh_boxplot.columns = ["Value", "Sample"]
Enh_boxplot["Sample"] = pd.Categorical(Enh_boxplot["Sample"], categories=["Ctrl", "MA", "SA"], ordered=True)

Fc_Enh1 = [Enh_boxplot[Enh_boxplot["Sample"] == "Ctrl"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "MA"]["Value"].mean(),
           Enh_boxplot[Enh_boxplot["Sample"] == "Ctrl"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "SA"]["Value"].mean(),
           Enh_boxplot[Enh_boxplot["Sample"] == "SA"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "MA"]["Value"].mean()]
print("Fold change in Enhancer-1:", Fc_Enh1)

# Figure 3 (TCs and Enhancer 2)

TCs = "chr4:74735101-74735247"
Enh = "chr4:74740108-74740484"

TC_mat = TCs_tpm_mat.loc[TCs, all_CTRL + all_MA + all_SA].to_frame().T
Enh_mat = Enhancers_tpm_mat.loc[Enh, all_CTRL + all_MA + all_SA].to_frame().T

Cor_plot = pd.concat([TC_mat, Enh_mat]).reset_index(drop=True)
Cor_plot["Sample"] = ["Ctrl"] * len(all_CTRL) + ["MA"] * len(all_MA) + ["SA"] * len(all_SA)

# Check correlation value
value_cor = prom_enha_cor_mat_all.loc[TCs, Enh]
print("Correlation:", value_cor)

# Check p-value
value_pvalue = prom_enha_cor_pval_mat_all.loc[TCs, Enh]
print("P-value:", value_pvalue)

# Correlation plot
g_Cor_Enh2 = sns.lmplot(data=Cor_plot, x="chr4:74735101-74735247", y="chr4:74740108-74740484",
                        hue="Sample", palette={"Ctrl": "darkred", "MA": "green", "SA": "gold"},
                        fit_reg=True, scatter_kws={"s": 50}, ci=None)
g_Cor_Enh2.set_axis_labels("Expression in promoter (TPM)", "Expression in enhancer (TPM)")
g_Cor_Enh2.fig.suptitle("Correlation = 0.74\nP-value = 2.067056e-07")
plt.show()

# Enhancer-2 Boxplot
Enh_boxplot = pd.concat([Enh_mat["chr4:74740108-74740484"], Cor_plot["Sample"]], axis=1)
Enh_boxplot.columns = ["Value", "Sample"]
Enh_boxplot["Sample"] = pd.Categorical(Enh_boxplot["Sample"], categories=["Ctrl", "MA", "SA"], ordered=True)

g_enh2 = sns.boxplot(data=Enh_boxplot, x="Sample", y="Value", palette={"Ctrl": "darkred", "MA": "green", "SA": "gold"})
g_enh2 = sns.stripplot(data=Enh_boxplot, x="Sample", y="Value", color="black", alpha=0.5)
g_enh2.set(xlabel="Group", ylabel="Enhancer expression (TPM)")
plt.title("Boxplot of Enhancer expression (TPM) in Enhancer-2")
plt.show()

# Fold change in Enhancer-2
Fc_Enh2 = [Enh_boxplot[Enh_boxplot["Sample"] == "Ctrl"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "MA"]["Value"].mean(),
           Enh_boxplot[Enh_boxplot["Sample"] == "Ctrl"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "SA"]["Value"].mean(),
           Enh_boxplot[Enh_boxplot["Sample"] == "SA"]["Value"].mean() / Enh_boxplot[Enh_boxplot["Sample"] == "MA"]["Value"].mean()]
print("Fold change in Enhancer-2:", Fc_Enh2)
