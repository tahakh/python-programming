
import pandas as pd
import numpy as np
import subprocess

# Load promoter and enhancer data
promoter_tpm = pd.read_csv("Pre-processing RData/Main_set/Remove_HBA_Set/RobustCAGE_tpm_IDchange.csv")
enhancer_tpm = pd.read_csv("Enhancer_Identification/Enhancer_8289/enhancer_output/Asthmaenhancer_tpm8289.csv")

# Define Control, MA, SA columns
all_CTRL = list(range(1, 10))
all_MA = list(range(10, 25))
all_SA = list(range(25, 38))

# Run BEDTools interactions
DIR_DATA = "/Users/tahakh/Tahmina_Akhter/Project_Asthma/Enhancer_Asthma/Scripts/Manuscript_Results/Interactions_Promoter_Enhancer/Input_data/"
subprocess.run(["bedtools", "window", "-a", f"{DIR_DATA}/CAGE_count_robustset_score.bed", "-b", f"{DIR_DATA}/Permissive_enhancer_8289.bed", "-l", "1000000", "-r", "1000000", ">", f"{DIR_DATA}/EPI_robustprm_permissiveEnh.txt"])

# Get data from Promoter-Enhancer Interactions
prom_enhan_inter = pd.read_csv("Interactions_Promoter_Enhancer/Input_data/EPI_robustprm_permissiveEnh.txt", sep="\t", header=None)
prom_enhan_inter.columns = ["TCs_ID", "Enh_ID"]
prom_enhan_inter["TCs_ID"] = prom_enhan_inter["TCs_ID"].apply(lambda x: "-".join(str(x).split("..")))

# Create a correlation matrix
prom_enhan_cor_mat = pd.DataFrame(index=prom_enhan_inter["TCs_ID"].unique(), columns=prom_enhan_inter["Enh_ID"].unique())
prom_enhan_pval_mat = pd.DataFrame(index=prom_enhan_inter["TCs_ID"].unique(), columns=prom_enhan_inter["Enh_ID"].unique())

for i, tc_id in enumerate(prom_enhan_cor_mat.index):
    for j, enh_id in enumerate(prom_enhan_cor_mat.columns):
        prom_enhan_cor_mat.iloc[i, j] = np.corrcoef(promoter_tpm.loc[tc_id], enhancer_tpm.loc[enh_id])[0, 1]
        _, prom_enhan_pval_mat.iloc[i, j] = scipy.stats.spearmanr(promoter_tpm.loc[tc_id], enhancer_tpm.loc[enh_id])

# Save correlation matrix
prom_enhan_cor_mat.to_csv("prom_enha_cor_mat.txt", sep="\t")
prom_enhan_pval_mat.to_csv("prom_enha_cor_pval_mat.txt", sep="\t")

# Calculate distance matrix
distance_matrix = pd.DataFrame(index=prom_enhan_inter["TCs_ID"].unique(), columns=prom_enhan_inter["Enh_ID"].unique())

for i, tc_id in enumerate(distance_matrix.index):
    for j, enh_id in enumerate(distance_matrix.columns):
        dist_vector = np.abs(np.array(prom_enhan_inter.loc[prom_enhan_inter["TCs_ID"] == tc_id, "Enh_ID"], dtype=float) - float(enh_id))
        distance_matrix.iloc[i, j] = np.min(dist_vector)

# Save distance matrix
distance_matrix.to_csv("distance_matrix.txt", sep="\t")
